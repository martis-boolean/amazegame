package baseGame;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/*
    @ project:  05. TestGame
    @ module:   old.SimpleGame
    @ created:  12/24/2018  
    @ by:       Ilya Azin
    @ version   v.1.0 
*/
public class Game extends Application {

    public final static double WINDOW_WIDTH = 1280;
    public final static double WINDOW_HEIGHT = 768;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("fxml/game.fxml"));
//        root.prefWidth(WINDOW_WIDTH);
//        root.prefHeight(WINDOW_HEIGHT);

        primaryStage.setTitle("MyGame");
        primaryStage.setScene(new Scene(root, WINDOW_WIDTH, WINDOW_HEIGHT));
//        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
