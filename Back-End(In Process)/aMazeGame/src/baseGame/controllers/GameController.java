package baseGame.controllers;

import baseGame.enums.Levels;
import baseGame.objects.components.Point2D;
import baseGame.objects.maze.Maze2D;
import baseGame.objects.units.Player;
import baseGame.objects.components.Tile;
import baseGame.utils.LayoutUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Box;


import java.net.URL;
import java.util.ResourceBundle;

/*
    @ project:  05. TestGame
    @ module:   GameController
    @ created:  12/24/2018  
    @ by:       Ilya Azin
    @ version   v.1.0 
*/
public class GameController implements Initializable {
    // TODO: -2 border -> +2 -> visible??
    @FXML
    private Pane paneLayout;

    @FXML
    private GridPane map;

    @FXML
    private ImageView player2;

    @FXML
    private ImageView selected;

    @FXML
    private Label tilePosition;

    @FXML
    private Box keyboardNodePlayer;

    @FXML
    private Box keyboardNode;

    private int i = 0;
    private int tileSize = 128;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // TODO: keyboardNode !!!
        //******** GAME INIT ********//
        // Init Maze
        Maze2D maze2D = new Maze2D(Levels.LEVEL_0, tileSize);
//        maze2D.fillField(new Point2D(1, 5), new Point2D(8, 4));
        // Init Player
        Player p1 = new Player(player2, tileSize, keyboardNodePlayer, tilePosition);

        // Run units
        p1.run(maze2D);

//        setTilePos(p1);
        //******** TESTS ********//
//        LayoutUtils.setKeyboardNode(keyboardNode);
//        keyboardNode.setOnKeyPressed(event -> {
//            setTilePos(p1);
//        });
        // TEST VI
/*        VisualItem v1 = new VisualItem(player2);
        System.out.println(v1.getLeft());
        System.out.println(v1.getRight());
        System.out.println(v1.getTop());
        System.out.println(v1.getBottom());*/
    }

    public void showTile(Tile tile) {
        LayoutUtils.setNode(selected, tile);
        selected.setVisible(true);
    }

}
