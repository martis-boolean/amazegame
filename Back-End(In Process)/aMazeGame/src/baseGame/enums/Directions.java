package baseGame.enums;


import baseGame.objects.components.Vector;

public enum Directions {
    UP      (new Vector(0,  -1)),
    DOWN    (new Vector(0,  1)),
    RIGHT   (new Vector(1,  0)),
    LEFT    (new Vector(-1, 0));

    private Vector direction;

    Directions(Vector direction) {
        this.direction = direction;
    }

    public Vector getDirection() {
        return direction;
    }
}
