package baseGame.enums;

import java.io.File;
import java.net.URL;

public enum Levels {
    LEVEL_0(0),
    LEVEL_1(1),
    LEVEL_2(2),
    LEVEL_3(3),
    LEVEL_4(4),
    LEVEL_5(5),
    LEVEL_6(6),
    LEVEL_7(7),
    LEVEL_8(8),
    LEVEL_9(9),
    LEVEL_10(10);

    private int id;

    Levels(int id) {
        this.id = id;
    }

    public File getLevel() {
        return new File("./src/baseGame/assets/level_" + id + ".map");
    }


}
