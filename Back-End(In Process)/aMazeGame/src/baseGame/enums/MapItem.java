package baseGame.enums;

public enum MapItem {
    // reachMessage -> reachMethod
    ENTRANCE    ('A', "GAME STARTED: Hello! Go to Exit!"),
    EXIT        ('B', "GAME COMPLETED: You've finished level!"),
    EMPTY       ('.', "*step*"),
    WALL        ('#', "GAME OVER: You've smashed!"),
    MONSTER     ('!', "GAME OVER: You've teared by monster!"),
    TRAP        ('@', "GAME OVER: You've caught!"),
    CHEESE      ('$', "*nyam*");

    private char id;
    private String reachMessage;


    MapItem(char id) {
        this(id, "");
    }
    MapItem(char id, String reachMessage) {
        this.id = id;
        this.reachMessage = reachMessage;
    }


    public char getId() {
        return id;
    }

    public String getReachMessage() {
        return reachMessage;
    }
}
