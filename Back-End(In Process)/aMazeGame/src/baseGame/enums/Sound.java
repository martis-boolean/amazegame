package baseGame.enums;

/*
    @ project:  aMazeGame
    @ module:   Sound
    @ created:  12/15/2018  
    @ by:       Ilya Azin
    @ version   v.1.0 
*/
public enum Sound {
    STEP("step.mp3");

    private String soundFile;

    Sound(String soundFile) {
        this.soundFile = soundFile;
    }

    public String getPath() {
        return "file:src/baseGame/assets/" + soundFile;
    }

    @Override
    public String toString() {
        return soundFile;
    }
}
