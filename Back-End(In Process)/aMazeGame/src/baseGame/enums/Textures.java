package baseGame.enums;

import javafx.scene.image.Image;

import java.nio.file.Paths;

public enum Textures {
    SELECTED("selected.jpg");


    private String imageFile;

    Textures(String imageFile) {
        this.imageFile = imageFile;
    }

    public Image getImage() {
        return new Image(Paths.get("src/assets/" + imageFile).toUri().toString());
    }

    @Override
    public String toString() {
        return imageFile;
    }
}
