package baseGame.interfaces;

public interface Measurable {
    double getLeft();
    double getRight();
    double getTop();
    double getBottom();
}
