package baseGame.interfaces;

import baseGame.enums.Directions;
import baseGame.objects.components.Point2D;

public interface Movable {
    void move(Directions directions);
    void move(Point2D point2D);
    void moveBySpeed();
}
