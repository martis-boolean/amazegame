package baseGame.interfaces.impl;

import baseGame.enums.MapItem;
import baseGame.objects.components.Point2D;
import baseGame.objects.components.Tile;

/*
    Created: 12/2/2018 by Ilya Azin
    @ version 1.0 
*/
public abstract class Maze {
    protected char[][][] field;
    protected int width;    // X
    protected int height;   // Y
    protected int depth;    // Z
    protected int tileSize;
    protected Point2D entrance;
    protected Point2D exit;

    // TODO: Point3D, Point2D -> Point3D ???
    // TODO: passes  !!!

    public Maze(char[][] field, int tileSize) {
        this.field = new char[1][field.length][field[0].length];
        this.field[0] = field;
        this.depth = 1;
        this.tileSize = tileSize;
        this.entrance = getPosition2D('A');
        this.exit =     getPosition2D('B');
        this.width =    field[0].length;
        this.height =   field.length;
    }

    public Maze(char[][][] field) {
        this.field = field;
    }


/*    private Point2D entrance;
    private Point2D exit;

    public Maze(Point2D entrance, Point2D exit) {
        this.entrance = entrance;
        this.exit = exit;
    }

    public Point2D getExit() {
        return exit;
    }*/

    //////// GETTERS && SETTERS \\\\\\\\
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getDepth() {
        return depth;
    }

    public Point2D getEntrance() {
        return entrance;
    }

    public Point2D getExit() {
        return exit;
    }

    //////// METHOD SUMMARY 2D \\\\\\\\
    // TODO: -> 3D
    // '.'  '🧀'  '#'  '□'  '🞓' ... //
    public boolean isMatching(MapItem item, Point2D pos) {
        return this.getValue(pos) == item.getId();
    }

    public void setItem(MapItem item, Point2D pos) {
        setItem(item.getId(), pos);
    }

    // Built-In
    protected char getValue(Point2D position) {
        // TODO: cellSize
        Tile tilePos = Tile.computeTile(position, 128);
        return field[depth - 1][(int) tilePos.getY()][(int) tilePos.getX()];
    }

    protected void setItem(char value, Point2D pos) {
        System.out.println(value);
        field[depth][(int) pos.getY()][(int) pos.getX()] = value;
    }

    // ? //
    protected Point2D getPosition2D(char value) {
        for (int k = 0; k < depth; k++) {
            for (int j = 0; j < height; j++) {
                for (int i = 0; i < width; i++) {
                    if (field[k][j][i] == value) {
                        return new Point2D(i, j);
                    }
                }
            }
        }
        return null;
    }
}

