package baseGame.interfaces.impl;

import baseGame.enums.Directions;
import baseGame.interfaces.Movable;
import baseGame.objects.components.Point2D;
import baseGame.objects.components.Tile;
import baseGame.objects.components.Vector;
import baseGame.utils.*;
import javafx.scene.Node;
import javafx.scene.image.ImageView;

/*
    @ project:  05. TestGame
    @ module:   MovableItem
    @ created:  12/24/2018  
    @ by:       Ilya Azin
    @ version   v.1.0 
*/
public abstract class MovableItem implements Movable, Stopable {
    // TODO: markerSIzes!
    // TODO: speed inc
    // TODO: move smoother
    protected Point2D position;
    //    private Tile position;
    protected Vector speed;
    protected Node marker;
    protected int tileSize;

    //////// CONSTRUCTOR \\\\\\\\
    public MovableItem(Node marker, Tile position) {
        this(marker, position.getTileLeftTop(), position.getTileSize());
    }

    public MovableItem(Node marker, Point2D position, int tileSize) {
        init(marker, tileSize);
        this.position = position;
        LayoutUtils.setNode(marker, position);
    }

    public MovableItem(Node marker, int tileSize) {
        init(marker, tileSize);
        this.position = new Point2D(marker.getLayoutX(), marker.getLayoutY());
    }

    private void init(Node marker, int tileSize) {
        this.marker = marker;
        this.speed = new Vector(0, 0);
        this.tileSize = tileSize;
        VFXManager.runStepAnimator(this);   // smooth moving and changing coordinates
    }

    //////// GETTERS && SETTERS \\\\\\\\
    public Point2D getPosition() {
        return position;
    }

    public Node getMarker() {
        return marker;
    }

    // MOVING
    @Override
    public void move(Directions direction) {
        String operation = direction.name();
        speed = new Vector(direction.getDirection());
        /* CHANGE OF COORDINATES IS REALIZED ON ANIMATOR BY SPEED */
        // * STEPS *
        /* DELAY SOUND!!! */
//        SoundManager.play(Sound.STEP, 0.2);
//        Logger.log(operation + speed);
    }

    // By tile...
    @Override
    public void move(Point2D point2D) {
        Tile pos = new Tile(position, 128);
        String posStr = pos.getTileCenter().toString();

        // init <Point2D> position
        pos = new Tile(point2D, pos.getTileSize(), pos.getTileType());
        // init marker position
        LayoutUtils.setNode(marker, pos);
        // log
        Logger.log(posStr + StringUtil.multiply(" ", 16 - posStr.length()) + " -> " + pos.getTileCenter());
    }

    // MOVING BY SPEED
    @Override
    public void moveBySpeed() {
        position = position.computeByVector(speed);
        LayoutUtils.setNode(marker, position);
    }

    // STOPPING
    @Override
    public void stop() {
        speed.setX(0);
        speed.setY(0);
    }
}
