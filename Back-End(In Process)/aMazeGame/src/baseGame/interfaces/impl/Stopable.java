package baseGame.interfaces.impl;

public interface Stopable {
    void stop();
}
