package baseGame.interfaces.impl;

import baseGame.objects.components.Path;
import baseGame.objects.components.Point2D;
import baseGame.objects.maze.Maze2D;
import javafx.scene.Node;

/*
    @ project:  05. TestGame
    @ module:   Unit
    @ created:  12/24/2018  
    @ by:       Ilya Azin
    @ version   v.1.0 
*/
public abstract class Unit extends MovableItem {
    public Unit(Node marker, int tileSize) {
        super(marker, tileSize);
    }

//    public abstract Path run(Maze2D maze);
}
