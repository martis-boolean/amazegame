package baseGame.interfaces.impl;

import baseGame.interfaces.Measurable;
import baseGame.objects.components.Point2D;
import javafx.scene.image.ImageView;

/*
    @ project:  05. TestGame
    @ module:   VisualItem
    @ created:  12/26/2018  
    @ by:       Ilya Azin
    @ version   v.1.0 
*/
public class VisualItem implements Measurable {
    // TODO: cast all marker to VI; VI is what???
    private ImageView vis;

    //////// CONSTRUCTOR \\\\\\\\
    public VisualItem(ImageView vis) {
        this.vis = vis;
    }
    //////// GETTERS \\\\\\\\
    public ImageView getVis() {
        return vis;
    }
    //////// GET SUMMARY \\\\\\\\
    @Override
    public double getLeft() {
        return vis.getLayoutX();
    }

    @Override
    public double getRight() {
        return vis.getLayoutX() + vis.getFitWidth();
    }

    @Override
    public double getTop() {
        return vis.getLayoutY();
    }

    @Override
    public double getBottom() {
        return vis.getLayoutY() + vis.getFitHeight();
    }

    public double getLayoutX() {
        return vis.getLayoutX();
    }

    public double getLayoutY() {
        return vis.getLayoutY();
    }

    public double getWidth() {
        return vis.getFitWidth();
    }

    public double getHeight() {
        return vis.getFitHeight();
    }

    public Point2D getCenter() {
        return new Point2D(getRight()-getLeft(), getBottom()-getTop());
    }

    public Point2D getPosition() {
        return new Point2D(vis.getLayoutX(), vis.getLayoutY());
    }
}
