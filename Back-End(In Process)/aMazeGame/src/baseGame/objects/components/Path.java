package baseGame.objects.components;

public class Path {
    private Point2D track[];
    private static final int PATH_BUFFER = 16;
    private int count;

    public Path() {
        this.track = new Point2D[PATH_BUFFER];
        this.count = 0;
    }

    public Path(Point2D[] track) {
        this.track = track;
        this.count = track.length;
    }

    public void add(int x, int y) {
        add(new Point2D(x, y));
    }

    public void add(Point2D point2D) {
        if (count == track.length) {
            extendPathBuffer();
        }

        track[count++] = point2D;
//        System.out.println("Added to Track: " + Point2D);
    }

    public void reverse() {
        for (int i = 0; i < count / 2; i++) {
            Point2D temp = track[i];
            track[i] = track[count - i - 1];
            track[count - i - 1] = temp;
        }
    }

    private void extendPathBuffer() {
        extendPathBuffer((int) (track.length * 1.5));
    }

    private void extendPathBuffer(int newLength) {
        Point2D old[] = track.clone();
        track = new Point2D[newLength];
        System.arraycopy(old, 0, track, 0, count);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("Path:\n");
        for (int i = 0; i < count; i++) {
            result.append(String.format("-> %s\n", track[i]));
        }
        return result.toString();
    }

    public static void main(String[] args) {
        Path path = new Path();
        path.add(0, 0);
        path.add(0, 1);
        path.add(1, 1);
        path.add(2, 1);
        path.add(3, 4);
        path.add(1, 3);

        System.out.println(path);
        path.reverse();
        System.out.println(path);
    }
}
