package baseGame.objects.components;

import baseGame.interfaces.impl.Point;

/*
    @ project:  05. TestGame
    @ module:   Point2D
    @ created:  12/24/2018  
    @ by:       Ilya Azin
    @ version   v.1.0 
*/
public class Point2D implements Point {
    // TODO: for 3D
    // TODO: genterics double / int ???
    protected double x;
    protected double y;

    //////// CONSTRUCTOR \\\\\\\\
    public Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    //////// GETTERS && SETTERS \\\\\\\\
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    //////// IMPLEMENTATIONS \\\\\\\\
    public boolean equals(Point2D other) {
        return (this.x == other.x) && (this.y == other.y);
    }

    public String toString() {
        return String.format("(%s, %s)", x, y);
    }
    //////// METHOD SUMMARY \\\\\\\\
    public Point2D computeByVector(Vector vector) {
        return new Point2D(this.getX() + vector.getX(), this.getY() + vector.getY());
    }

    public Point2D[] getClosest() {
        int DIRECTION_AMOUNT = 4;
        Point2D result[] = new Point2D[DIRECTION_AMOUNT];

        // All available directions initializing
        Vector directions[] = new Vector[]{
                new Vector(1, 0),
                new Vector(0, -1),
                new Vector(-1, 0),
                new Vector(0, 1)
        };

        for (int i = 0; i < DIRECTION_AMOUNT; i++) {
            result[i] = this.computeByVector(directions[i]);
        }
        return result;
    }

    public Point2D getIntPoint(Point2D point2D) {
        return new Point2D((int) point2D.x, (int) point2D.y);
    }

    //////// PRIVATE \\\\\\\\
}
