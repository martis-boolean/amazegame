package baseGame.objects.components;

import baseGame.interfaces.impl.Point;

/*
    @ project:  05. TestGame
    @ module:   Point3D
    @ created:  12/26/2018  
    @ by:       Ilya Azin
    @ version   v.1.0 
*/
public class Point3D implements Point {
    protected double x;
    protected double y;
    private double z;

    //////// CONSTRUCTOR \\\\\\\\
    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    //////// GETTERS && SETTERS \\\\\\\\
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }
    //////// IMPLEMENTATIONS \\\\\\\\
    public boolean equals(Point3D other) {
        return (this.x == other.x) && (this.y == other.y) && (this.z == other.z);
    }

    public String toString() {
        return String.format("(%s, %s, %s)", x, y, z);
    }
    //////// METHOD SUMMARY \\\\\\\\

    //////// PRIVATE \\\\\\\\
}
