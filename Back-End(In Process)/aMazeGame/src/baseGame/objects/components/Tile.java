package baseGame.objects.components;

import baseGame.enums.TileType;
import baseGame.interfaces.impl.Point;
import baseGame.interfaces.impl.VisualItem;

import java.util.Arrays;

/*
    @ project:  05. TestGame
    @ module:   Tile
    @ created:  12/24/2018  
    @ by:       Ilya Azin
    @ version   v.1.0 
*/
public class Tile extends Point2D {
    private int tileSize;
    private TileType tileType;

    //////// CONSTRUCTOR \\\\\\\\
    public Tile(Point2D pos, int tileSize) {
        this(pos, tileSize, TileType.GROUND);
    }

    public Tile(Point2D pos, int tileSize, TileType tileType) {
        this((int) pos.x, (int) pos.y, tileSize, tileType);
    }

    public Tile(int x, int y, int tileSize, TileType tileType) {
        super(x, y);
        this.tileSize = tileSize;
        this.tileType = tileType;
    }
    //////// GETTERS && SETTERS \\\\\\\\

    public int getTileSize() {
        return tileSize;
    }

    public TileType getTileType() {
        return tileType;
    }

    public void setWall() {
        tileType = TileType.WALL;
    }

    //////// METHOD SUMMARY \\\\\\\\
    // Compute next tile by vector
    @Override
    public Tile computeByVector(Vector vector) {
        Point2D newPoint = super.computeByVector(vector);
        return new Tile(newPoint, tileSize, tileType);
    }

    // Compute tile basis coordinates
    public Point2D getTileCenter() {
        return new Point2D((x + 0.5) * tileSize, (y + 0.5) * tileSize);
    }

    public Point2D getTileLeftTop() {
        Point2D center = getTileCenter();
        return new Point2D(center.x - tileSize / 2, center.y - tileSize / 2);
    }

    public Point2D getTileRightBottom() {
        Point2D center = getTileCenter();
        return new Point2D(center.x + tileSize / 2, center.y + tileSize / 2);
    }

    //////// STATIC SUMMARY \\\\\\\\
    // Get tile by coordinates
    public static Tile computeTile(Point2D pos, int tileSize) {
        // alpha, beta; 0.5 < i < 1.5; 1.5 < i < 2.5; ...
        pos = new Point2D(Math.floor(pos.x / tileSize), Math.floor(pos.y / tileSize));
        return new Tile(pos, tileSize);
    }

    public static boolean isValid(VisualItem vi, int tileSize) {
        Tile possible = computeTile(new Point2D(vi.getLayoutX(), vi.getLayoutY()), tileSize);
        boolean isValidLeft = possible.getTileLeftTop().x - vi.getLeft() <= 0;
        boolean isValidTop = possible.getTileLeftTop().y - vi.getTop() <= 0;
        boolean isValidRight = possible.getTileRightBottom().x - vi.getRight() >= 0;
        boolean isValidBottom = possible.getTileRightBottom().y - vi.getBottom() >= 0;

        return isValidLeft && isValidTop && isValidRight && isValidBottom;
    }

    public static boolean areIntersected(VisualItem vi, Tile tile) {
        System.out.println(computeTile(vi.getPosition(), tile.tileSize));
        boolean isIntersectedH = (tile.tileSize + vi.getWidth()) - (tile.getTileLeftTop().getX() - vi.getRight()) >= 0;
        boolean isIntersectedV = (tile.tileSize + vi.getHeight()) - (tile.getTileLeftTop().getY() - vi.getBottom()) >= 0;
        // TODO: ???ц
        return isIntersectedH || isIntersectedV;
    }

    // Detect collision
    public static boolean computeCollision(VisualItem vi, int tileSize) {
        Tile possible = computeTile(new Point2D(vi.getLayoutX(), vi.getLayoutY()), tileSize);
        Point2D[] closest = possible.getClosest();
        for (Point2D x : closest) {
            if (areIntersected(vi, new Tile(x, tileSize))) {
                System.out.println(x);
                return true;
            }
        }
        return false;
    }

    ////////////////////// TESTS \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public static void main(String[] args) {
        System.out.println(Tile.computeTile(new Point2D(0, 0), 5));
        System.out.println(Tile.computeTile(new Point2D(2, 4), 5));
        System.out.println(Tile.computeTile(new Point2D(5, 5), 5));
        System.out.println(Tile.computeTile(new Point2D(6, 7), 5));
        System.out.println(Tile.computeTile(new Point2D(7, 2), 5));
        System.out.println(Tile.computeTile(new Point2D(3, 7), 5));
        System.out.println(Tile.computeTile(new Point2D(13, 13), 5));
    }
}
