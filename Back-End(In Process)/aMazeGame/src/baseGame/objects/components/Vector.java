package baseGame.objects.components;

/*
    @ project:  05. TestGame
    @ module:   Vector
    @ created:  12/24/2018  
    @ by:       Ilya Azin
    @ version   v.1.0 
*/
public class Vector {
    private double x;
    private double y;

    public Vector(Vector vector) {
        this.x = vector.x;
        this.y = vector.y;
    }

    public Vector(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return String.format("(%s, %s)", x, y);
    }
}
