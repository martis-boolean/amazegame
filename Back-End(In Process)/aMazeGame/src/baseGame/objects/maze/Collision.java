package baseGame.objects.maze;

import baseGame.objects.components.Point2D;

/*
    Created: 12/2/2018 by Ilya Azin
    @ version 1.0 
*/
public class Collision extends Point2D {
    private boolean isBreakable;
    private boolean isBroken;

    public Collision(int x, int y) {
        this(x, y, false);
    }

    public Collision(int x, int y, boolean isBreakable) {
        super(x, y);
        this.isBreakable = isBreakable;
        this.isBroken = false;
    }

    public boolean isBreakable() {
        return isBreakable;
    }

    public boolean isBroken() {
        return isBroken;
    }

}
