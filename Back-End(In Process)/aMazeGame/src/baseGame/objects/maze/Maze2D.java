package baseGame.objects.maze;

import baseGame.enums.Levels;
import baseGame.enums.MapItem;
import baseGame.interfaces.impl.Maze;
import baseGame.objects.components.Path;
import baseGame.objects.components.Point2D;
import baseGame.utils.ReadUtil;
import baseGame.utils.StringUtil;

public class Maze2D extends Maze {
    // By load map...
    public Maze2D(Levels level, int tileSize) {
        this(ReadUtil.loadMap(level.getLevel()), tileSize);
    }

    public Maze2D(Point2D entrance, Point2D exit, int width, int height, int tileSize) {
        this(Maze2D.setPassPoints(entrance, exit, width, height), tileSize);
    }

    public Maze2D(char[][] field, int tileSize) {
        // TODO: isValidArray
        super(field, tileSize);
        System.out.println(this);
    }

    /* AlgorithmLee */
    public void fillField(Point2D start, Point2D finish) {
        fillField(new Point2D[]{start}, start, finish, 1);
    }

    private void fillField(Point2D[] queue, Point2D start, Point2D finish, int waveCount) {
        // TODO: Lvl 2
        int SIDES_AMOUNT = 4;
        Point2D newQueue[] = new Point2D[SIDES_AMOUNT * SIDES_AMOUNT];
        int count = 0;

        // BruteForce by LastVisitedCells
        for (Point2D cell : queue) {
            // if endOfQueueReached
            if (cell == null) {
                break;
                // if finishCellReached
            } else if (cell.equals(finish)) {
                return;
                // checking closest cells
            } else {
                for (Point2D nextTile : cell.getClosest()) {
                    if (isValid(nextTile)
                            && this.getValue(nextTile) == MapItem.EMPTY.getId()
                            && !(nextTile.equals(start) && waveCount == 2)) {
                        this.setItem((char) (waveCount + 48), nextTile);
                        newQueue[count++] = nextTile;
                    }
                }
            }
        }
        // Next Wave
        fillField(newQueue, start, finish, ++waveCount);
    }

    private boolean isValid(Point2D cell) {
        return (0 <= cell.getX() && cell.getX() < width) && (0 <= cell.getY() && cell.getY() < height);
    }

    private static char[][] setPassPoints(Point2D entrance, Point2D exit, int width, int height) {
        char[][] tempField = new char[height][width];
        tempField[(int) entrance.getY()][(int) entrance.getX()] = 'A';
        tempField[(int) exit.getY()][(int) exit.getX()] = 'B';
        return tempField;
    }

    public Path showPath(Path path) {
        Path result = new Path();
        return new Path();
    }


    @Override
    public String toString() {
        return toString(new Point2D(-1, -1));
    }

    public String toString(Point2D playerPos) {
        // TODO: normalize
        /**
         * 🧀 -3   '$' - cheese cell   -> eat
         * ⛝ -2   '@' - trap cell     -> died
         * #  -1   '#' - wall cell     -> died
         * * 0..   '.' - walkable cell -> move
         */
        // COLORS
        String grey = "\u001B[90m";
        String yellowBold = "\u001B[33m";
        String end = "\u001B[0m";
        // ICONS
        String trap = "⛝\u2009";
        String wall = grey + "#" + end;
        String player = "\uD83D\uDC2D\u2009";
        String cheese = yellowBold + "\uD83E\uDDC0\u2009" + end;
        // VARIABLES
        StringBuilder result = new StringBuilder();
        int CELL_SIZE = 1; // TODO: customizing
        String CELL_BORDER = StringUtil.multiply(wall, CELL_SIZE);
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                String vCurCell;
                // if Player is Here
                if (playerPos.equals(new Point2D(j, i))) {
                    vCurCell = player;
                } else {
                    char x = field[0][i][j];
                    if (x == MapItem.TRAP.getId()) {
                        vCurCell = trap;
                    } else if (x == MapItem.WALL.getId()) {
                        vCurCell = CELL_BORDER;
                    } else if (x == MapItem.CHEESE.getId()) {
                        vCurCell = cheese;
                    } else {
                        vCurCell = Character.toString(field[0][i][j]);
                    }
                }
                result.append(vCurCell + " ");
            }
            result.append("\n");
        }
        return result.toString();
    }


}
