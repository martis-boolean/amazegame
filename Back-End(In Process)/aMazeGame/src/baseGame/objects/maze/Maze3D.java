package baseGame.objects.maze;

import baseGame.interfaces.impl.Maze;

/*
    Created: 12/2/2018 by Ilya Azin
    @ version 1.0 
*/
public class Maze3D extends Maze {
    public Maze3D(char[][][] field) {
        super(field);
    }
}
