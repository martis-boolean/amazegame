package baseGame.objects.units;

import baseGame.interfaces.impl.Unit;
import baseGame.objects.components.Path;
import baseGame.objects.components.Point2D;
import baseGame.objects.maze.Maze2D;
import javafx.scene.shape.Circle;

/*
    Created: 12/2/2018 by Ilya Azin
    @ version 1.0 
*/
public class Monster extends Unit {

    public Monster(Circle marker, int tileSize) {
        super(marker, tileSize);
    }
    public Path run(Maze2D maze) {
        // Algorithm Lee
        return new Path();
    }
}
