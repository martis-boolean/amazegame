package baseGame.objects.units;

import baseGame.enums.Directions;
import baseGame.enums.MapItem;
import baseGame.interfaces.impl.Unit;
import baseGame.interfaces.impl.VisualItem;
import baseGame.objects.components.Path;
import baseGame.objects.components.Tile;
import baseGame.objects.maze.Maze2D;
import baseGame.utils.LayoutUtils;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.shape.Box;

/*
    @ project:  05. TestGame
    @ module:   Player
    @ created:  12/24/2018  
    @ by:       Ilya Azin
    @ version   v.1.0 
*/
public class Player extends Unit {
    private Maze2D location;
    private Path track = new Path();
    private Label display;
    private Box keyboardNode;

    // TEMPORARY: Label
    public Player(Node marker, int tileSize, Box keyboardNode, Label display) {
        super(marker, tileSize);
        this.display = display;
        this.keyboardNode = keyboardNode;
    }

    public void run(Maze2D maze) {
        System.out.println("Player run...");
        // init KeyListener
        LayoutUtils.setKeyboardNode(keyboardNode, this::run, event -> this.stop());
        this.location = maze;
    }

    public void run(KeyEvent event) {
        // TODO: computeTileByVector
        switch (event.getCode()) {
            case W:
                this.move(Directions.UP);
                break;
            case A:
                this.move(Directions.LEFT);
                break;
            case S:
                this.move(Directions.DOWN);
                break;
            case D:
                this.move(Directions.RIGHT);
                break;
        }
        showInfo();
        computeSituations();
    }

    ////////// PRIVATE \\\\\\\\\\
    private void computeSituations() {
        MapItem[] gameOver = {MapItem.WALL, MapItem.TRAP, MapItem.MONSTER};
        MapItem[] gameInProcess = {MapItem.EMPTY};
        boolean wallReached = Tile.computeCollision(new VisualItem((ImageView) marker), tileSize);
        if (wallReached) {
//            System.err.println(MapItem.WALL.getReachMessage());
//            System.exit(0);
            System.out.println("wall reached!");
//            stop();
        }

/*        for (MapItem x : MapItem.values()) {
            if (location.isMatching(x, position)) {
                if (ArrayUtils.contains(x, gameOver)) {
                    System.err.println(x.getReachMessage());
                    System.exit(0);
                }
            }
        }*/
    }
    private void showInfo() {
        Tile pos = Tile.computeTile(position, this.tileSize);
        // Log
        boolean isValid = Tile.isValid(new VisualItem((ImageView) marker), tileSize);    // TODO: VERY BAD!!!
        display.setText(pos.toString() + "\n" + isValid);
    }
    private void setKeyboardNode(Box keyboardNode) {
        // attaching KeyEvent
        keyboardNode.setFocusTraversable(true);
        keyboardNode.requestFocus();
        keyboardNode.setOnKeyPressed(this::run);
        keyboardNode.setOnKeyReleased(event -> this.stop());
    }
}
