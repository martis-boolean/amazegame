package baseGame.utils;

/*
    @ project:  05. TestGame
    @ module:   ArrayUtils
    @ created:  12/26/2018  
    @ by:       Ilya Azin
    @ version   v.1.0 
*/
public class ArrayUtils {
    //////// CONSTRUCTOR \\\\\\\\
    //////// GETTERS && SETTERS \\\\\\\\
    //////// IMPLEMENTATIONS \\\\\\\\
    //////// METHOD SUMMARY \\\\\\\\
    public static boolean contains(Object o, Object[] array) {
        for (Object x : array) {
            if (o.equals(x)) return true;
        }
        return false;
    }
    //////// PRIVATE \\\\\\\\
}
