package baseGame.utils;

import baseGame.objects.components.Point2D;
import baseGame.objects.components.Tile;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.KeyEvent;
import javafx.scene.shape.Box;


/*
    @ project:  05. TestGame
    @ module:   LayoutUtils
    @ created:  12/25/2018  
    @ by:       Ilya Azin
    @ version   v.1.0 
*/
public class LayoutUtils {
    public static void setNode(Node e, Tile position) {
        setNode(e, position.getTileCenter().getX(), position.getTileCenter().getY());
    }

    public static void setNode(Node e, Point2D position) {
        setNode(e, position.getX(), position.getY());
    }

    public static void setNode(Node e, double x, double y) {
        e.setLayoutY(y);
        e.setLayoutX(x);
    }

    public static void setKeyboardNode(Box keyboardNode) {
        setKeyboardNode(keyboardNode, null, null);
    }

    public static void setKeyboardNode(Box keyboardNode, EventHandler<KeyEvent> keyPressed, EventHandler<KeyEvent> keyReleased) {
        // attaching KeyEvent
        keyboardNode.setFocusTraversable(true);
        keyboardNode.requestFocus();
        if (keyPressed != null) {
            keyboardNode.setOnKeyPressed(keyPressed);
        }
        if (keyPressed != null) {
            keyboardNode.setOnKeyReleased(keyReleased);
        }
    }

}
