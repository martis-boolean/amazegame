package baseGame.utils;

/*
    @ project:  05. TestGame
    @ module:   Logger
    @ created:  12/24/2018  
    @ by:       Ilya Azin
    @ version   v.1.0 
*/
public class Logger {
    private static final String PREFIX = "LOGGER";

    public static void log(String message) {
        log(message, true);
    }
    public static void log(String message, boolean nextLine) {
        System.out.print(PREFIX + ": " + message + (nextLine ? "\n" : ""));
    }
}
