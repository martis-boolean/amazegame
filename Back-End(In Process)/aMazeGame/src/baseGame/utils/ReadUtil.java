package baseGame.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

/*
    @ project:  05. TestGame
    @ module:   ReadUtil
    @ created:  12/26/2018  
    @ by:       Ilya Azin
    @ version   v.1.0 
*/
public class ReadUtil {
    public static char[][] loadMap(File map) {
        try {
            // TODO: char reader
            Scanner scanner = new Scanner(map);
            String[] basis = scanner.nextLine().split(" ");
            int width = Integer.parseInt(basis[0]);
            int height = Integer.parseInt(basis[1]);

            char[][] result = new char[height][];
//
            for (int j = 0; j < height; j++) {
//                System.out.println(scanner.nextLine());
                result[j] = scanner.next().toCharArray();
            }

//            System.out.println(Arrays.deepToString(result));
//
            return result;

        } catch (FileNotFoundException e) {
            System.out.println(map + " NOT FOUND!!!");
//            e.printStackTrace();
        }
        return new char[][]{};
    }
}
