package baseGame.utils;

import baseGame.enums.Sound;
import javafx.scene.media.AudioClip;

/*
    @ project:  aMazeGame
    @ module:   SoundManager
    @ created:  12/24/2018
    @ by:       Ilya Azin
    @ version   v.1.0
*/
public class SoundManager {
    // TODO: incVolume, ...
    private static AudioClip currentSound;


    /////// SOUND METHOD SUMMARY \\\\\\\
    public static void play(Sound soundFile, double volume) {
        // boolean f: while old sound....play old sound..
/*        if (currentSound != null) {
            currentSound.stop();
        }*/

        currentSound = new AudioClip(soundFile.getPath());
        currentSound.play(volume);
    }

    public static void stop() {
        currentSound.stop();
    }

}

