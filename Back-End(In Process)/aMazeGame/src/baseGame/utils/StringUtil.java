package baseGame.utils;

public class StringUtil {
    public static String setRank(int number) {
        return setRank(number, 2);
    }

    public static String setRank(int number, int rank) {
        return multiply("0", rank - Integer.toString(number).length()) + Integer.toString(number);
    }

    public static String multiply(String a, int count) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < count; i++) {
            result.append(a);
        }
        return result.toString();
    }

    public static String borderH(String unitBorder, int count) {
        return multiply(unitBorder, count);
    }

    public static void main(String[] args) {
        System.out.println(setRank(5));
        System.out.println(setRank(13));
        System.out.println(setRank(113));
    }
}
