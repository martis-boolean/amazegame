package old.animation;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AnimGame extends Application {
    static final int WIDTH = 600;
    static final int HEIGHT = 400;


    @Override
    public void start(Stage primaryStage) throws Exception{
        // create scene
        Group root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Scene scene = new Scene(root, WIDTH, HEIGHT);

/*        // Circle creating
        Circle c1 = new Circle(ballX, ballY, ballRadius, Color.BLUE);
        root.getChildren().add(c1);*/


        // create GUI
        primaryStage.setTitle("TestGame v.0.0");
        primaryStage.setScene(scene);
        primaryStage.show();


    }


    public static void main(String[] args) {
        launch(args);
    }
}
