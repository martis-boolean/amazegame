package old.animation;

import javafx.animation.AnimationTimer;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Box;
import javafx.scene.shape.Circle;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable, EventHandler<KeyEvent>{

    double ballRadius = 40;
    double ballX = AnimGame.WIDTH / 2;
    double ballY = AnimGame.HEIGHT / 2;
    double xSpeed = 0;
    double ySpeed = 0;
    AnimationTimer animator;

    @FXML
    private StackPane rootPane;

    @FXML
    private Circle shapeCircle;

    @FXML
    private Box keyboardNode;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        shapeCircle.setLayoutX(ballX);
        shapeCircle.setLayoutY(ballY);

        // attaching KeyEvent
        keyboardNode.setFocusTraversable(true);
        keyboardNode.requestFocus();
        keyboardNode.setOnKeyPressed(this);
        keyboardNode.setOnKeyReleased(ev -> {
            xSpeed = 0;
            ySpeed = 0;
        });

        run();
    }

    @Override
    public void handle(KeyEvent event) {
//        double x = (xSpeed > 0) ? ballX + ballRadius : ballX - ballRadius;
//        double y = (ySpeed > 0) ? ballY + ballRadius : ballY - ballRadius;
        System.out.println(event);
        switch (event.getCode()) {
            case W:
                ySpeed -= 1;
//                run(ballX, ballY - ballRadius);
                break;
            case A:
                xSpeed -= 1;
//                run(ballX - ballRadius, ballY);
                break;
            case S:
                ySpeed += 1;
//                run(ballX, ballY + ballRadius);
                break;
            case D:
                xSpeed += 1;
//                run(ballX + ballRadius, ballY);
                break;
        }
    }

    private void run() {
        System.out.print("(" + ballX +"; " + ballY + ")    <- ");
        System.out.println("(dX:" + xSpeed +"; dY: " + ySpeed + ")");
//        System.out.println(x + " " + y);
        // AnimationTimer
        animator = new AnimationTimer() {
            @Override
            public void handle(long now) {
/*                if (ballX == x && xSpeed != 0|| ballY == y && ySpeed != 0) {
                    this.stop();
                    xSpeed = 0;
                    ySpeed = 0;
                }*/

                // update
                ballX += xSpeed;
                ballY += ySpeed;
                // render
                shapeCircle.setLayoutX(ballX);
                shapeCircle.setLayoutY(ballY);
            }
        };

        animator.start();
    }
}
