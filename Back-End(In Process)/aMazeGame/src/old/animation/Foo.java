package old.animation;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Box;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class Foo extends Application implements EventHandler<KeyEvent> {
    private final int WIDTH = 600;
    private final int HEIGHT = 400;

    double ballRadius = 40;
    double ballX = 100;
    double ballY = 200;
    double xSpeed = 4;
    @Override
    public void start(Stage primaryStage) throws Exception{
        // create scene
        Group root = new Group();
        Scene scene = new Scene(root, WIDTH, HEIGHT);
        // Circle creating
        Circle c1 = new Circle(ballX, ballY, ballRadius, Color.BLUE);
        root.getChildren().add(c1);
        // attaching KeyEvent
        final Box keyboardNode = new Box();
        keyboardNode.setFocusTraversable(true);
        keyboardNode.requestFocus();
        keyboardNode.setOnKeyPressed(this);
        root.getChildren().add(keyboardNode);

        // create GUI
        primaryStage.setTitle("TestGame v.0.0");
        primaryStage.setScene(scene);
        primaryStage.show();

        // AnimationTimer
        AnimationTimer animator = new AnimationTimer() {
            @Override
            public void handle(long now) {
                // update
                ballX += xSpeed;
                if (ballX + ballRadius >= WIDTH) {
                    ballX = WIDTH - ballRadius;
                    xSpeed *= -1;
                } else if (ballX - ballRadius < 0) {
                    ballX = 0 + ballRadius;
                    xSpeed *= -1;
                }

                // render
                c1.setCenterX(ballX);
            }
        };

        animator.start();
    }


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void handle(KeyEvent event) {
        if (event.getCode() == KeyCode.A) {
            xSpeed *= -1;
        }
    }
}
