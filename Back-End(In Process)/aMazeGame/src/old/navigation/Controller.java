package old.navigation;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Box;
import javafx.scene.shape.Circle;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable, EventHandler<KeyEvent> {

    double ballRadius = 40;
    double ballX = NaviGame.WIDTH / 2;
    double ballY = NaviGame.HEIGHT / 2;
    double xSpeed = 60;
    double ySpeed = 60;

    @FXML
    private StackPane rootPane;

    @FXML
    private Circle shapeCircle;

    @FXML
    private Box keyboardNode;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println();
        shapeCircle.setCenterX(ballX);
        shapeCircle.setCenterY(ballY);

        System.out.println("ti pidor");

        // attaching KeyEvent
        keyboardNode.setFocusTraversable(true);
        keyboardNode.requestFocus();
        keyboardNode.setOnKeyPressed(this);

    }

    @Override
    public void handle(KeyEvent event) {
        if (event.getCode() == KeyCode.W) {
            System.out.println("UP");
            ballY = Math.max(0, ballY - ySpeed);
        } else if (event.getCode() == KeyCode.A) {
            System.out.println("LEFT");
            ballX = Math.max(0, ballX - xSpeed);
        } else if (event.getCode() == KeyCode.S) {
            System.out.println("DOWN");
            ballY = Math.min(NaviGame.HEIGHT, ballY + ySpeed);
        } else if (event.getCode() == KeyCode.D) {
            System.out.println("RIGHT");
            ballX = Math.min(NaviGame.WIDTH, ballX + xSpeed);
        }

        shapeCircle.setCenterX(ballX);
        shapeCircle.setCenterY(ballY);
    }
}
