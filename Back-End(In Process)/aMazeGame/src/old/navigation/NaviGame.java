package old.navigation;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class NaviGame extends Application {
    static final int WIDTH = 600;
    static final int HEIGHT = 400;


    @Override
    public void start(Stage primaryStage) throws Exception{
        // create scene
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Scene scene = new Scene(root, WIDTH, HEIGHT);



        // create GUI
        primaryStage.setTitle("TestGame v.0.0: navigating");
        primaryStage.setScene(scene);
        primaryStage.show();


    }


    public static void main(String[] args) {
        launch(args);
    }
}
