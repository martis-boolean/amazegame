package ru.itis.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import ru.itis.enums.Layouts;
import ru.itis.enums.Music;
import ru.itis.interfaces.impl.DeterminedController;
import ru.itis.utils.MusicManager;
import ru.itis.utils.ScreenManager;
import ru.itis.utils.VFXManager;

import java.net.URL;
import java.util.ResourceBundle;

/*
    @ project:  aMazeGame
    @ module:   GameController
    @ created:  12/11/2018
    @ by:       Ilya Azin
    @ version   v.1.0
*/
public class GameController extends DeterminedController implements Initializable {


    @FXML
    private StackPane stackPaneLayout;

    @FXML
    private Button myBtn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // TODO: switch music
        VFXManager.delay(1, ev -> MusicManager.play(Music.FIGHT_BG));
    }

    // SWITCHING SCENES
    public void goToMainMenu(MouseEvent actionEvent) {
        ScreenManager.setScreen(Layouts.MAIN_MENU);
    }
}
