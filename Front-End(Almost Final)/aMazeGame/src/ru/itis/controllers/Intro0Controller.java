package ru.itis.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import ru.itis.enums.Layouts;
import ru.itis.utils.ScreenManager;
import ru.itis.utils.VFXManager;

import java.net.URL;
import java.util.ResourceBundle;

/*
    @ project:  aMazeGame
    @ module:   Intro0Controller
    @ created:  12/11/2018  
    @ by:       Ilya Azin
    @ version   v.1.0 
*/
public class Intro0Controller implements Initializable {
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        VFXManager.delay(0.5, ev -> loadNextScene());
    }

    @FXML
    private void loadNextScene() {
        ScreenManager.setScreen(Layouts.INTRO_1);
    }
}
