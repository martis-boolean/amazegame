package ru.itis.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import ru.itis.enums.Layouts;
import ru.itis.utils.ScreenManager;
import ru.itis.utils.VFXManager;

import java.net.URL;
import java.util.ResourceBundle;

/*
    @ project:  aMazeGame
    @ module:   Intro1Controller
    @ created:  12/11/2018  
    @ by:       Ilya Azin
    @ version   v.1.0 
*/
public class Intro1Controller implements Initializable {
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        VFXManager.delay(3, ev -> loadNextScene());
    }

    @FXML
    private void loadNextScene() {
        ScreenManager.setScreen(Layouts.INTRO_2);
    }
}
