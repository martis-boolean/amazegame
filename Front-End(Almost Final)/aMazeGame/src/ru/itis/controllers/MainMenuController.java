package ru.itis.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;

import ru.itis.enums.Layouts;
import ru.itis.enums.MainMenuTheme;
import ru.itis.interfaces.impl.DeterminedController;
import ru.itis.objects.User;
import ru.itis.utils.MusicManager;
import ru.itis.utils.ScreenManager;
import ru.itis.utils.VFXManager;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;


/*
    @ project:  aMazeGame
    @ module:   MainMenuController
    @ created:  12/11/2018
    @ by:       Ilya Azin
    @ version   v.1.0
*/
public class MainMenuController extends DeterminedController implements Initializable {

    @FXML
    private StackPane stackPaneLayout;

    @FXML
    private ImageView ibtnMusic;

    @FXML
    private ImageView ibtnMusicLight;

    @FXML
    private ImageView imgBack;

    @FXML
    private Button btnThemeSwitcher;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        VFXManager.delay(1, ev -> setUserTheme());
    }

    // SWITCHING SCENES
    public void goToMapChoicer(ActionEvent actionEvent) {
        ScreenManager.setScreen(Layouts.MAP_CHOICER);
    }

    public void goToSettings(ActionEvent actionEvent) {
        ScreenManager.setScreen(Layouts.SETTINGS);
    }

    public void goToCredits(ActionEvent actionEvent) {
        ScreenManager.setScreen(Layouts.CREDITS);
    }

    public void goToDesktop(ActionEvent actionEvent) {
        // TODO: ~modal WIndow
        System.out.println("Come Back!");
        System.exit(0);
    }

    // SWITCHING ON/OFF MUSIC
    public void switchMusicState(MouseEvent mouseEvent) {
        ImageView imageView =   (ImageView) mouseEvent.getSource();
        Image musicOff =        new Image("assets/buttons/h_btnMusicOff.png");
        Image musicOn =         new Image("assets/buttons/h_btnMusicOn.png");

        User.wishMusic ^= true;
        if (User.wishMusic) {
            MusicManager.play();
            imageView.setImage(musicOn);
            System.out.println("\uD83D\uDD6C: " + "on");
        } else {
            MusicManager.pause();
            imageView.setImage(musicOff);
            System.out.println("\uD83D\uDD6C: " + "off");
        }
    }

    // SWITCHING THEMES
    @FXML
    private void switchTheme(ActionEvent actionEvent) {
        User.themeID = (User.themeID < MainMenuTheme.size() - 1) ? User.themeID + 1 : 0;
        setTheme(MainMenuTheme.values()[User.themeID]);
    }

    private void setUserTheme() {
        if (User.themeID == -1) {
            User.themeID = new Random().nextInt(MainMenuTheme.size());
        }

        setTheme(User.getTheme());
    }

    private void setTheme(MainMenuTheme theme) {
        imgBack.setImage(theme.getImage());
        if (User.wishMusic) {
            MusicManager.play(theme.getMusic());
        }

        // other...
        String styleWinter =    "-fx-text-fill: white;";
        String styleBase =      "-fx-text-fill: #6c5934;";
        btnThemeSwitcher.setStyle((theme.name().equals("WINTER")) ? styleWinter : styleBase);
    }
/*    // ICOBUTTON FX
    public void fadeOutFrame(MouseEvent mouseEvent) {
        fade(ibtnMusicLight, 1.0, 0.0, 0.9);
    }

    public void fadeInFrame(MouseEvent mouseEvent) {
        fade(ibtnMusicLight, 0.0, 1.0, 0.5);
    }*/
}
