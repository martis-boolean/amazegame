package ru.itis.enums;

import javafx.scene.image.Image;

import java.nio.file.Paths;

public enum Buttons {
    ;


    private String imageFile;

    Buttons(String imageFile) {
        this.imageFile = imageFile;
    }

    public Image getImage() {
        return new Image(Paths.get("src/assets/buttons/" + imageFile).toUri().toString());
    }

    @Override
    public String toString() {
        return imageFile;
    }
}
