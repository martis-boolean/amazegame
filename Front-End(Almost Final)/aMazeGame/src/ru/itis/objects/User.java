package ru.itis.objects;

import ru.itis.enums.MainMenuTheme;

/*
    @ project:  aMazeGame
    @ module:   User
    @ created:  12/23/2018  
    @ by:       Ilya Azin
    @ version   v.1.0 
*/
public class User {
    // GRAPHICS
    public final static int WINDOW_WIDTH = 1280;
    public final static int WINDOW_HEIGHT = 720;

    // MUSIC
    public static boolean wishMusic = true;

    // THEMES
    public static int themeID = -1;

    public static MainMenuTheme getTheme() {
        return MainMenuTheme.values()[themeID];
    }
    // BUILT-IN
    //    public static boolean inMenuHierarchy = false;
}
