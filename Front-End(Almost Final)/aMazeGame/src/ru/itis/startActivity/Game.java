package ru.itis.startActivity;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import ru.itis.enums.Layouts;
import ru.itis.objects.User;
import ru.itis.utils.ScreenManager;
import ru.itis.utils.VFXManager;

import java.io.IOException;

/**
 * .......
 * INTRO0 -> INTRO1 -> INTRO2 -> INTRO3 -> MAIN_MENU
 * .......
 * MAIN_MENU <-> MAP_CHOICER -> GAME
 * MAIN_MENU <-> SETTINGS
 * MAIN_MENU <-> CREDITS
 * MAIN_MENU -> $DESKTOP
 * .......
 * GAME -> MAIN_MENU
 * .......
 */
public class Game extends Application {

    @Override

    public void start(Stage primaryStage){
        // TODO: README file
        // Set First Screen
        ScreenManager.setScreen(Layouts.INTRO_0);
        // Set First Layout
        Group root = new Group();
        root.getChildren().addAll(ScreenManager.getMainLayout());
        // Set First Stage
        primaryStage.setScene(new Scene(root, User.WINDOW_WIDTH, User.WINDOW_HEIGHT, Color.BLACK));
        primaryStage.setTitle("aMazeGame v.1.0");
//        primaryStage.setFullScreen(true);
//        primaryStage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

}
