package ru.itis.utils;

import java.awt.event.MouseEvent;

/*
    @ project:  aMazeGame
    @ module:   ButtonManager
    @ created:  12/22/2018  
    @ by:       Ilya Azin
    @ version   v.1.0 
*/
public class ButtonManager {
    // TODO: not merge interfaces
    public void actionEntered(MouseEvent mouseEvent) {
        System.out.println("Hey");
    }
    // class 1
    // actionEntered(Node, class)
    // actionExited(Node, class)
    // actionPressed(Node, class)

    // class 2
    // actionEntered(Node, class)
    // actionExited(Node, class)
    // actionPressed(Node, class)

    // class 3
    // actionEntered(Node, class)
    // actionExited(Node, class)
    // actionPressed(Node, class)
}
